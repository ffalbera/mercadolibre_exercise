package com.exercisemercadolibre.ui.amount;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;

import com.exercisemercadolibre.R;
import com.exercisemercadolibre.ui.payment_method.PaymentMethodActivity;
import com.exercisemercadolibre.utils.Constants;

public class AmountActivity extends AppCompatActivity implements View.OnClickListener, AmountContract.View, TextWatcher {
    private Button btNext;
    private TextInputLayout tiLayout;
    private TextInputEditText tiAmount;
    private AmountPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amount);

        getSupportActionBar().setTitle(getString(R.string.amount_receivable_title));

        tiLayout = findViewById(R.id.ti_layout_amount);

        tiAmount = findViewById(R.id.ti_amount);
        tiAmount.addTextChangedListener(this);

        btNext = findViewById(R.id.bt_amount_next);
        btNext.setOnClickListener(this);

        presenter = new AmountPresenter(this);
        verifyDataToShowDialog();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.bt_amount_next) {
            double amount = Double.valueOf(tiAmount.getText().toString());
            nextActivity(amount);
        }
    }

    @Override
    public void disableButtonNext() {
        tiLayout.setError(getString(R.string.error_amount_enter));
        btNext.setEnabled(false);
    }

    @Override
    public void enableButtonNext() {
        btNext.setEnabled(true);
        tiLayout.setError(Constants.EMPTY_STRING);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        presenter.verifyAmount(charSequence.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        System.exit(0);
    }

    public void verifyDataToShowDialog() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            final double amount = extras.getDouble(Constants.KEY_AMOUNT);
            final String paymentMethodName = extras.getString(Constants.KEY_PAYMENT_METHOD_NAME);
            final String bankName = extras.getString(Constants.KEY_BANK_NAME);
            final String recommendedMessage = extras.getString(Constants.KEY_RECOMMENDED_MESSAGE);

            if (paymentMethodName != null && bankName != null && recommendedMessage != null) {
                showDialogData(amount, paymentMethodName, bankName, recommendedMessage);
            }
        }
    }

    public void showDialogData(double amount, @NonNull String paymentMethodName, @NonNull String bankName, @NonNull String recommendedMessage) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getString(R.string.currency_import, String.valueOf(amount) + Constants.JUMP));
        stringBuilder.append(getString(R.string.currency_payment_method, paymentMethodName) + Constants.JUMP);
        stringBuilder.append(getString(R.string.currency_bank, bankName) + Constants.JUMP);
        stringBuilder.append(getString(R.string.currency_fee_amount, recommendedMessage) + Constants.JUMP);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.dialog_title));
        builder.setCancelable(false);
        builder.setMessage(stringBuilder.toString())
                .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        builder.show();
    }

    private void nextActivity(double amount) {
        Intent paymentMethodActivity = new Intent(this, PaymentMethodActivity.class);
        paymentMethodActivity.putExtra(Constants.KEY_AMOUNT, amount);
        startActivity(paymentMethodActivity);
        finish();
    }

}
