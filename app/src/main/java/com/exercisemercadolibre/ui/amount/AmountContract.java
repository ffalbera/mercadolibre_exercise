package com.exercisemercadolibre.ui.amount;

public interface AmountContract {

    interface View {

        void disableButtonNext();

        void enableButtonNext();
    }

    interface Presenter {

        void verifyAmount(String amount);
    }
}
