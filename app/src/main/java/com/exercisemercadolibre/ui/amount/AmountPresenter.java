package com.exercisemercadolibre.ui.amount;

public class AmountPresenter implements AmountContract.Presenter {

    private AmountContract.View view;

    AmountPresenter(AmountContract.View view) {
        this.view = view;
    }

    @Override
    public void verifyAmount(String amount) {
        if (amount.isEmpty()) {
            view.disableButtonNext();
        } else {
            view.enableButtonNext();
        }
    }
}
