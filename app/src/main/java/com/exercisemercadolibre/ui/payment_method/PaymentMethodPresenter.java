package com.exercisemercadolibre.ui.payment_method;

import com.exercisemercadolibre.model.PaymentMethodResponse;

public class PaymentMethodPresenter implements PaymentMethodContract.Presenter, PaymentMethodContract.Interactor.OnFinishedListener {

    private PaymentMethodContract.Interactor interactor;
    private PaymentMethodContract.View view;

    public PaymentMethodPresenter(PaymentMethodContract.View view) {
        this.view = view;
        this.interactor = new PaymentMethodInteractor();
    }

    @Override
    public void onSucessfull(PaymentMethodResponse paymentMethodResponse) {
        if (paymentMethodResponse.isEmpty()) {
            view.showNotResultPaymentMethod();
        } else {
            view.setPaymentMethodList(paymentMethodResponse);
        }
        view.hideProgress();
    }

    @Override
    public void onFailure(Throwable t) {
        view.onResponseFailure(t);
        view.hideProgress();
        view.showNotResultPaymentMethod();
    }


    @Override
    public void retrievePaymentMethodList() {
        view.showProgress();
        interactor.getPaymentMethods(this);
    }
}
