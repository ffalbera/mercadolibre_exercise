package com.exercisemercadolibre.ui.payment_method;

import android.util.Log;

import com.exercisemercadolibre.network.ApiClient;
import com.exercisemercadolibre.network.ApiInterface;
import com.exercisemercadolibre.model.PaymentMethodResponse;
import com.exercisemercadolibre.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.exercisemercadolibre.network.ApiClient.API_KEY;

public class PaymentMethodInteractor implements PaymentMethodContract.Interactor {

    @Override
    public void getPaymentMethods(final OnFinishedListener onFinishedListener) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<PaymentMethodResponse> call = apiService.getPaymentMethod(API_KEY);
        call.enqueue(new Callback<PaymentMethodResponse>() {
            @Override
            public void onResponse(Call<PaymentMethodResponse> call, Response<PaymentMethodResponse> response) {
                Log.d(Constants.TAG_PAYMENT_METOHD_MODEL, Constants.TAG_SUCCESSFUL);
                if (onFinishedListener != null) {
                    onFinishedListener.onSucessfull(response.body());
                }
            }

            @Override
            public void onFailure(Call<PaymentMethodResponse> call, Throwable t) {
                Log.d(Constants.TAG_PAYMENT_METOHD_MODEL, Constants.TAG_FAILED + t.getMessage());
                if (onFinishedListener != null) {
                    onFinishedListener.onFailure(t);
                }
            }
        });
    }
}
