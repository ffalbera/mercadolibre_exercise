package com.exercisemercadolibre.ui.payment_method;

import com.exercisemercadolibre.model.PaymentMethodResponse;

public interface PaymentMethodContract {

    interface Interactor {

        interface OnFinishedListener {
            void onSucessfull(PaymentMethodResponse paymentMethodResponse);

            void onFailure(Throwable t);
        }

        void getPaymentMethods(OnFinishedListener onFinishedListener);

    }

    interface View {

        void showProgress();

        void hideProgress();

        void setPaymentMethodList(PaymentMethodResponse paymentMethodResponse);

        void showNotResultPaymentMethod();

        void onResponseFailure(Throwable throwable);

    }

    interface Presenter {

        void retrievePaymentMethodList();

    }

}
