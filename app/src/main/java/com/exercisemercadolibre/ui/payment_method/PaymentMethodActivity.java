package com.exercisemercadolibre.ui.payment_method;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.exercisemercadolibre.R;
import com.exercisemercadolibre.ui.adapter.PaymentMethodAdapter;
import com.exercisemercadolibre.ui.amount.AmountActivity;
import com.exercisemercadolibre.model.PaymentMethod;
import com.exercisemercadolibre.model.PaymentMethodResponse;
import com.exercisemercadolibre.ui.banks.BanksActivity;
import com.exercisemercadolibre.utils.Constants;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class PaymentMethodActivity extends AppCompatActivity implements PaymentMethodContract.View {

    private PaymentMethodPresenter presenter;
    private ProgressBar pbLoading;
    private RecyclerView rvDataList;
    private PaymentMethodAdapter adapter;
    private TextView tvImport;
    private TextView tvNotDataFound;
    private double amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);

        getSupportActionBar().setTitle(getString(R.string.payment_methods));

        tvImport = findViewById(R.id.tv_payment_method_import);
        pbLoading = findViewById(R.id.pb_payment_method_loading);

        tvNotDataFound = findViewById(R.id.tv_payment_method_empty);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            amount = bundle.getDouble(Constants.KEY_AMOUNT);
            tvImport.setText(getString(R.string.currency_import, String.format("%.2f", amount)));
        }

        rvDataList = findViewById(R.id.rv_payment_method_list);
        rvDataList.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        List<PaymentMethod> paymentMethodList = new ArrayList<>();
        adapter = new PaymentMethodAdapter(this, paymentMethodList);
        rvDataList.setAdapter(adapter);

        presenter = new PaymentMethodPresenter(this);
        presenter.retrievePaymentMethodList();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bundle bundle = data.getExtras();
        if (bundle != null) {
            amount = bundle.getDouble(Constants.KEY_AMOUNT);
            tvImport.setText(getString(R.string.currency_import, String.format("%.2f", amount)));
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent bankActivity = new Intent(this, AmountActivity.class);
        Bundle bundle = new Bundle();
        bankActivity.putExtras(bundle);
        startActivity(bankActivity);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress() {
        pbLoading.setVisibility(VISIBLE);
    }

    @Override
    public void hideProgress() {
        pbLoading.setVisibility(GONE);
    }

    @Override
    public void setPaymentMethodList(PaymentMethodResponse paymentMethodResponse) {
        List<PaymentMethod> paymentMethodList = new ArrayList<>();
        Iterator<PaymentMethod> iter = paymentMethodResponse.iterator();
        while (iter.hasNext()) {
            PaymentMethod paymentMethod = iter.next();
            paymentMethodList.add(paymentMethod);
        }
        adapter.swapData(paymentMethodList);
    }

    @Override
    public void showNotResultPaymentMethod() {
        tvNotDataFound.setVisibility(VISIBLE);
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Log.e(Constants.TAG_FAILED, throwable.getMessage());
        Toast.makeText(this, getString(R.string.communication_error), Toast.LENGTH_LONG).show();
    }

    public void onPaymentItemClick(PaymentMethod paymentMethod) {
        Intent bankActivity = new Intent(this, BanksActivity.class);
        Bundle bundle = new Bundle();
        bundle.putDouble(Constants.KEY_AMOUNT, amount);
        bundle.putString(Constants.KEY_PAYMENT_METHOD_ID, paymentMethod.getId());
        bundle.putString(Constants.KEY_PAYMENT_METHOD_NAME, paymentMethod.getName());
        bankActivity.putExtras(bundle);
        startActivity(bankActivity);
        finish();
    }


}
