package com.exercisemercadolibre.ui.fee_amount;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.exercisemercadolibre.R;
import com.exercisemercadolibre.ui.adapter.FeeAmountAdapter;
import com.exercisemercadolibre.ui.amount.AmountActivity;
import com.exercisemercadolibre.ui.banks.BanksActivity;
import com.exercisemercadolibre.model.FeeAmount;
import com.exercisemercadolibre.model.FeeAmountResponse;
import com.exercisemercadolibre.model.PayerCostsItem;
import com.exercisemercadolibre.utils.Constants;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class FeeAmountActivity extends AppCompatActivity implements FeeAmountContract.View {

    private FeeAmountPresenter presenter;
    private ProgressBar pbLoading;
    private RecyclerView rvDataList;
    private FeeAmountAdapter adapter;
    private TextView tvNotDataFound;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_amount);

        getSupportActionBar().setTitle(getString(R.string.fee_amount_title));

        TextView tvImport = findViewById(R.id.tv_fee_import_pay);
        TextView tvPaymentMethodName = findViewById(R.id.tv_fee_payment_method_name);
        TextView tvBankName = findViewById(R.id.tv_fee_bank_name);
        tvNotDataFound = findViewById(R.id.tv_fee_empty);

        pbLoading = findViewById(R.id.pb_fee_loading);

        rvDataList = findViewById(R.id.rv_fee_list);
        rvDataList.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        List<PayerCostsItem> payerCostsItems = new ArrayList<>();
        adapter = new FeeAmountAdapter(this, payerCostsItems);
        rvDataList.setAdapter(adapter);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            final double amount = bundle.getDouble(Constants.KEY_AMOUNT);
            final String paymentMethodName = bundle.getString(Constants.KEY_PAYMENT_METHOD_NAME);
            final String paymentMethodId = bundle.getString(Constants.KEY_PAYMENT_METHOD_ID);
            final int bankId = bundle.getInt(Constants.KEY_BANK_ID);
            final String bankName = bundle.getString(Constants.KEY_BANK_NAME);

            tvImport.setText(getString(R.string.currency_import, String.format("%.2f", amount)));
            tvPaymentMethodName.setText(getString(R.string.currency_payment_method, paymentMethodName));
            tvBankName.setText(getString(R.string.currency_bank, bankName));

            presenter = new FeeAmountPresenter(this);
            presenter.retrieveFeeAmountList(amount, paymentMethodId, bankId);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent bankActivity = new Intent(this, BanksActivity.class);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            bankActivity.putExtras(bundle);
        }
        startActivity(bankActivity);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress() {
        pbLoading.setVisibility(VISIBLE);
    }

    @Override
    public void hideProgress() {
        pbLoading.setVisibility(GONE);
    }

    @Override
    public void setFeeAmountList(FeeAmountResponse feeAmountResponse) {
        List<PayerCostsItem> payerCostsItems = new ArrayList<>();
        Iterator<FeeAmount> iter = feeAmountResponse.iterator();
        while (iter.hasNext()) {
            FeeAmount feeAmount = iter.next();
            payerCostsItems.addAll(feeAmount.getPayerCosts());
        }
        adapter.swapData(payerCostsItems);
    }

    @Override
    public void showNotResultFeeAmount() {
        tvNotDataFound.setVisibility(VISIBLE);
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Log.e(Constants.TAG_FAILED, throwable.getMessage());
        Toast.makeText(this, getString(R.string.communication_error), Toast.LENGTH_LONG).show();
    }

    public void onFeeAmountItemClick(PayerCostsItem payerCostsItem) {
        Intent amountActivty = new Intent(this, AmountActivity.class);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            bundle.putString(Constants.KEY_RECOMMENDED_MESSAGE, payerCostsItem.getRecommendedMessage());
            amountActivty.putExtras(bundle);
        }
        startActivity(amountActivty);
        finish();
    }


}
