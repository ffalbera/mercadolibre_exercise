package com.exercisemercadolibre.ui.fee_amount;

import android.support.annotation.NonNull;

import com.exercisemercadolibre.model.FeeAmountResponse;

public class FeeAmountPresenter implements FeeAmountContract.Presenter, FeeAmountContract.Interactor.OnFinishedListener {

    private FeeAmountContract.Interactor interactor;
    private FeeAmountContract.View view;

    public FeeAmountPresenter(FeeAmountContract.View view) {
        this.view = view;
        this.interactor = new FeeAmountInteractor();
    }

    @Override
    public void onSucessfull(FeeAmountResponse feeAmountResponse) {
        if (feeAmountResponse.isEmpty()) {
            view.showNotResultFeeAmount();
        } else {
            view.setFeeAmountList(feeAmountResponse);
        }
        view.hideProgress();
    }

    @Override
    public void onFailure(Throwable t) {
        view.onResponseFailure(t);
        view.hideProgress();
        view.showNotResultFeeAmount();
    }

    @Override
    public void retrieveFeeAmountList(double importToPay, @NonNull String paymentMethodId, int issuerId) {
        view.showProgress();
        interactor.getFeeAmounts(importToPay, paymentMethodId, issuerId, this);
    }
}
