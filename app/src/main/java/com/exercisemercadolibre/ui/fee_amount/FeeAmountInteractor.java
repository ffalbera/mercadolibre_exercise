package com.exercisemercadolibre.ui.fee_amount;

import android.support.annotation.NonNull;
import android.util.Log;

import com.exercisemercadolibre.model.FeeAmountResponse;
import com.exercisemercadolibre.network.ApiClient;
import com.exercisemercadolibre.network.ApiInterface;
import com.exercisemercadolibre.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.exercisemercadolibre.network.ApiClient.API_KEY;

public class FeeAmountInteractor implements FeeAmountContract.Interactor {

    @Override
    public void getFeeAmounts(double importToPay, @NonNull String paymentMethodId, int issuerId, final OnFinishedListener onFinishedListener) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<FeeAmountResponse> call = apiService.getFeeAmounts(API_KEY, importToPay, paymentMethodId, issuerId);
        call.enqueue(new Callback<FeeAmountResponse>() {
            @Override
            public void onResponse(Call<FeeAmountResponse> call, Response<FeeAmountResponse> response) {
                Log.d(Constants.TAG_FEE_AMOUNT, Constants.TAG_SUCCESSFUL);
                if (onFinishedListener != null) {
                    onFinishedListener.onSucessfull(response.body());
                }

            }

            @Override
            public void onFailure(Call<FeeAmountResponse> call, Throwable t) {
                Log.d(Constants.TAG_FEE_AMOUNT, Constants.TAG_FAILED + t.getMessage());
                if (onFinishedListener != null) {
                    onFinishedListener.onFailure(t);
                }
            }
        });
    }
}
