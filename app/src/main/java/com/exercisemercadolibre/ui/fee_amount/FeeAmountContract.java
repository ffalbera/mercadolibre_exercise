package com.exercisemercadolibre.ui.fee_amount;

import android.support.annotation.NonNull;

import com.exercisemercadolibre.model.FeeAmountResponse;

public interface FeeAmountContract {

    interface Interactor {

        interface OnFinishedListener {
            void onSucessfull(FeeAmountResponse feeAmountResponse);

            void onFailure(Throwable t);
        }

        void getFeeAmounts(double importToPay, @NonNull String paymentMethodId, int issuerId, OnFinishedListener onFinishedListener);

    }

    interface View {

        void showProgress();

        void hideProgress();

        void setFeeAmountList(FeeAmountResponse feeAmountResponse);

        void showNotResultFeeAmount();

        void onResponseFailure(Throwable throwable);

    }

    interface Presenter {

        void retrieveFeeAmountList(double importToPay, @NonNull String paymentMethodId, int issuerId);

    }

}
