package com.exercisemercadolibre.ui.banks;

import android.support.annotation.NonNull;

import com.exercisemercadolibre.model.BankResponse;

public interface BankContract {

    interface Interactor {

        interface OnFinishedListener {
            void onSucessfull(BankResponse bankResponse);

            void onFailure(Throwable t);
        }

        void getBanks(@NonNull String paymentMethodId, OnFinishedListener onFinishedListener);

    }

    interface View {

        void showProgress();

        void hideProgress();

        void setBankList(BankResponse bankResponse);

        void showNotResultBank();

        void onResponseFailure(Throwable throwable);

    }

    interface Presenter {

        void retrieveBankList(@NonNull String paymentMethodId);

    }

}
