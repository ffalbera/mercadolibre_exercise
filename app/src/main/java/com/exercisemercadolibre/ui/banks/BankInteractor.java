package com.exercisemercadolibre.ui.banks;

import android.support.annotation.NonNull;
import android.util.Log;

import com.exercisemercadolibre.model.BankResponse;
import com.exercisemercadolibre.network.ApiClient;
import com.exercisemercadolibre.network.ApiInterface;
import com.exercisemercadolibre.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.exercisemercadolibre.network.ApiClient.API_KEY;

public class BankInteractor implements BankContract.Interactor {

    @Override
    public void getBanks(@NonNull String paymentMethodId, final OnFinishedListener onFinishedListener) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<BankResponse> call = apiService.getBanks(API_KEY, paymentMethodId);
        call.enqueue(new Callback<BankResponse>() {
            @Override
            public void onResponse(Call<BankResponse> call, Response<BankResponse> response) {
                Log.d(Constants.TAG_BANK_MODEL, Constants.TAG_SUCCESSFUL);
                if (onFinishedListener != null) {
                    onFinishedListener.onSucessfull(response.body());
                }
            }

            @Override
            public void onFailure(Call<BankResponse> call, Throwable t) {
                Log.d(Constants.TAG_BANK_MODEL, Constants.TAG_FAILED + t.getMessage());
                if (onFinishedListener != null) {
                    onFinishedListener.onFailure(t);
                }
            }
        });
    }
}
