package com.exercisemercadolibre.ui.banks;

import android.support.annotation.NonNull;

import com.exercisemercadolibre.model.BankResponse;

public class BankPresenter implements BankContract.Presenter, BankContract.Interactor.OnFinishedListener {

    private BankContract.Interactor interactor;
    private BankContract.View view;

    BankPresenter(@NonNull BankContract.View view) {
        this.view = view;
        this.interactor = new BankInteractor();
    }

    @Override
    public void onSucessfull(BankResponse bankResponse) {
        if (bankResponse.isEmpty()) {
            view.showNotResultBank();
        } else {
            view.setBankList(bankResponse);
        }
        view.hideProgress();
    }

    @Override
    public void onFailure(Throwable t) {
        view.onResponseFailure(t);
        view.hideProgress();
    }

    @Override
    public void retrieveBankList(@NonNull String paymentMethodId) {
        view.showProgress();
        interactor.getBanks(paymentMethodId, this);
    }
}
