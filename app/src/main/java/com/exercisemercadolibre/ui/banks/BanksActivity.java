package com.exercisemercadolibre.ui.banks;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.exercisemercadolibre.R;
import com.exercisemercadolibre.ui.adapter.BankAdapter;
import com.exercisemercadolibre.model.Bank;
import com.exercisemercadolibre.model.BankResponse;
import com.exercisemercadolibre.ui.fee_amount.FeeAmountActivity;
import com.exercisemercadolibre.ui.payment_method.PaymentMethodActivity;
import com.exercisemercadolibre.utils.Constants;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class BanksActivity extends AppCompatActivity implements BankContract.View {

    private BankPresenter presenter;
    private ProgressBar pbLoading;
    private RecyclerView rvDataList;
    private BankAdapter adapter;
    private TextView tvNotDataFound;
    private double amount;
    private String paymentMethodName;
    private String paymentMethodId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banks);

        getSupportActionBar().setTitle(getString(R.string.bank_title));

        TextView tvImport = findViewById(R.id.tv_bank_payment_method_import);
        TextView tvPaymentMethodName = findViewById(R.id.tv_bank_payment_method_name);
        tvNotDataFound = findViewById(R.id.tv_bank_empty);

        pbLoading = findViewById(R.id.pb_bank_loading);

        rvDataList = findViewById(R.id.rv_banks_list);
        rvDataList.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        List<Bank> bankList = new ArrayList<>();
        adapter = new BankAdapter(this, bankList);
        rvDataList.setAdapter(adapter);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            amount = bundle.getDouble(Constants.KEY_AMOUNT);
            paymentMethodId = bundle.getString(Constants.KEY_PAYMENT_METHOD_ID);
            paymentMethodName = bundle.getString(Constants.KEY_PAYMENT_METHOD_NAME);

            tvImport.setText(getString(R.string.currency_import, String.format("%.2f", amount)));
            tvPaymentMethodName.setText(getString(R.string.currency_payment_method, paymentMethodName));

            presenter = new BankPresenter(this);
            presenter.retrieveBankList(paymentMethodId);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent bankActivity = new Intent(this, PaymentMethodActivity.class);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            bankActivity.putExtras(bundle);
        }
        startActivity(bankActivity);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress() {
        pbLoading.setVisibility(VISIBLE);
    }

    @Override
    public void hideProgress() {
        pbLoading.setVisibility(GONE);
    }

    @Override
    public void setBankList(BankResponse bankResponse) {
        List<Bank> bankList = new ArrayList<>();
        Iterator<Bank> iter = bankResponse.iterator();
        while (iter.hasNext()) {
            Bank bank = iter.next();
            bankList.add(bank);
        }
        adapter.swapData(bankList);
    }

    @Override
    public void showNotResultBank() {
        tvNotDataFound.setVisibility(VISIBLE);
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Log.e(Constants.TAG_FAILED, throwable.getMessage());
        Toast.makeText(this, getString(R.string.communication_error), Toast.LENGTH_LONG).show();
    }

     public void onBankItemClick(Bank bank) {
        Intent bankActivity = new Intent(this, FeeAmountActivity.class);
        Bundle bundle = new Bundle();
        bundle.putDouble(Constants.KEY_AMOUNT, amount);
        bundle.putString(Constants.KEY_PAYMENT_METHOD_ID, paymentMethodId);
        bundle.putString(Constants.KEY_PAYMENT_METHOD_NAME, paymentMethodName);
        bundle.putInt(Constants.KEY_BANK_ID, Integer.valueOf(bank.getId()));
        bundle.putString(Constants.KEY_BANK_NAME, bank.getName());
        bankActivity.putExtras(bundle);
        startActivity(bankActivity);
        finish();
    }


}
