package com.exercisemercadolibre.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.exercisemercadolibre.R;
import com.exercisemercadolibre.ui.banks.BanksActivity;
import com.exercisemercadolibre.model.Bank;
import java.util.List;

public class BankAdapter extends RecyclerView.Adapter<BankAdapter.BankViewHolder> {

    private Context context;
    private List<Bank> banks;

    public BankAdapter(@NonNull Context ctx, @NonNull List<Bank> data) {
        this.context = ctx;
        this.banks = data;
    }

    public void swapData(List<Bank> data) {
        this.banks.clear();
        this.banks.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BankViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_data, parent, false);
        return new BankViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BankViewHolder holder, final int position) {
        final Bank bank = banks.get(position);
        holder.tvDataName.setText(bank.getName());
        // loading image using Glide library
        Glide.with(context)
                .load(bank.getThumbnail())
                .into(holder.ivDataThumb);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BanksActivity) context).onBankItemClick(bank);
            }
        });
    }

    @Override
    public int getItemCount() {
        return banks.size();
    }


    class BankViewHolder extends RecyclerView.ViewHolder {
        TextView tvDataName;
        ImageView ivDataThumb;

        BankViewHolder(View itemView) {
            super(itemView);

            tvDataName = itemView.findViewById(R.id.name_text);
            ivDataThumb = itemView.findViewById(R.id.image_img);
        }
    }

}
