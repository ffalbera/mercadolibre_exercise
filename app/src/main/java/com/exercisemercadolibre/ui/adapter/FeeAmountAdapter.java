package com.exercisemercadolibre.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.exercisemercadolibre.R;
import com.exercisemercadolibre.ui.fee_amount.FeeAmountActivity;
import com.exercisemercadolibre.model.PayerCostsItem;

import java.util.List;

public class FeeAmountAdapter extends RecyclerView.Adapter<FeeAmountAdapter.BankViewHolder> {

    private Context context;
    private List<PayerCostsItem> payerCostsItems;

    public FeeAmountAdapter(@NonNull Context ctx, @NonNull List<PayerCostsItem> data) {
        this.context = ctx;
        this.payerCostsItems = data;
    }

    public void swapData(List<PayerCostsItem> data) {
        this.payerCostsItems.clear();
        this.payerCostsItems.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BankViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_data, parent, false);
        return new BankViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BankViewHolder holder, final int position) {
        final PayerCostsItem payerCostsItem = payerCostsItems.get(position);
        holder.tvDataName.setText(payerCostsItem.getRecommendedMessage());
        holder.ivDataThumb.setVisibility(View.GONE);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((FeeAmountActivity) context).onFeeAmountItemClick(payerCostsItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return payerCostsItems.size();
    }


    class BankViewHolder extends RecyclerView.ViewHolder {
        TextView tvDataName;
        ImageView ivDataThumb;

        BankViewHolder(View itemView) {
            super(itemView);

            tvDataName = itemView.findViewById(R.id.name_text);
            ivDataThumb = itemView.findViewById(R.id.image_img);
        }
    }

}
