package com.exercisemercadolibre.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.exercisemercadolibre.R;
import com.exercisemercadolibre.model.PaymentMethod;
import com.exercisemercadolibre.ui.payment_method.PaymentMethodActivity;

import java.util.List;

public class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.PaymentMethodViewHolder> {

    private Context context;
    private List<PaymentMethod> payments;

    public PaymentMethodAdapter(@NonNull Context ctx, @NonNull List<PaymentMethod> data) {
        this.context = ctx;
        this.payments = data;
    }

    public void swapData(List<PaymentMethod> data) {
        this.payments.clear();
        this.payments.addAll(data);
        notifyDataSetChanged();
    }

    public PaymentMethod getPaymentMethod(@NonNull int position) {
        return payments.get(position);
    }

    @NonNull
    @Override
    public PaymentMethodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_data, parent, false);
        return new PaymentMethodViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentMethodViewHolder holder, final int position) {
        final PaymentMethod paymentMethod = payments.get(position);
        holder.tvDataName.setText(paymentMethod.getName());
        // loading image using Glide library
        Glide.with(context)
                .load(paymentMethod.getThumbnail())
                .into(holder.ivDataThumb);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((PaymentMethodActivity) context).onPaymentItemClick(paymentMethod);
            }
        });
    }

    @Override
    public int getItemCount() {
        return payments.size();
    }


    class PaymentMethodViewHolder extends RecyclerView.ViewHolder {
        TextView tvDataName;
        ImageView ivDataThumb;

        PaymentMethodViewHolder(View itemView) {
            super(itemView);

            tvDataName = itemView.findViewById(R.id.name_text);
            ivDataThumb = itemView.findViewById(R.id.image_img);
        }
    }

}
