package com.exercisemercadolibre.utils;

public class Constants {

    public static final String TAG_PAYMENT_METOHD_MODEL = "PaymentMethodModel";
    public static final String TAG_BANK_MODEL = "BankModel";
    public static final String TAG_FEE_AMOUNT = "FeeAmountModel";
    public static final String TAG_SUCCESSFUL = "Successful";
    public static final String TAG_FAILED = "Failed";
    public static final String KEY_AMOUNT = "amount";
    public static final String KEY_PAYMENT_METHOD_ID = "payment_method_id";
    public static final String KEY_PAYMENT_METHOD_NAME = "payment_method_name";
    public static final String KEY_BANK_ID = "bank_id";
    public static final String KEY_BANK_NAME = "bank_name";
    public static final String KEY_RECOMMENDED_MESSAGE = "recommended_message";
    public static final String EMPTY_STRING = "";
    public static final String JUMP = "\n";

    // Constants for test
    public static final String TEST_PAYMENT_METHOD_ID = "visa";
    public static final String TEST_PAYMENT_METHOD_NAME = "VISA";
    public static final String TEST_BANK_NAME = "Banco Hipotecario";
    public static final String TEST_RECOMMENDED_MESSAGE = "1 cuota de $ 100,00 ($ 100,00)";
    public static final String TEST_AMOUNT = "36";
    public static final double TEST_AMOUNT_DOUBLE = 36;

}
