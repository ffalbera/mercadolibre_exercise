package com.exercisemercadolibre.network;

import com.exercisemercadolibre.model.BankResponse;
import com.exercisemercadolibre.model.FeeAmountResponse;
import com.exercisemercadolibre.model.PaymentMethodResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("payment_methods/")
    Call<PaymentMethodResponse> getPaymentMethod(@Query("public_key") String apiKey);

    @GET("payment_methods/card_issuers/")
    Call<BankResponse> getBanks(@Query("public_key") String apiKey,
                                @Query("payment_method_id") String paymentMethodId);

    @GET("payment_methods/installments/")
    Call<FeeAmountResponse> getFeeAmounts(@Query("public_key") String apiKey,
                                          @Query("amount") double amount,
                                          @Query("payment_method_id") String paymentMethodId,
                                          @Query("issuer.id") int isserId);

}
