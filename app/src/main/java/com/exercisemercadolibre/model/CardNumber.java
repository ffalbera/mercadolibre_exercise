package com.exercisemercadolibre.model;

import com.google.gson.annotations.SerializedName;

public class CardNumber{

	@SerializedName("length")
	private int length;

	@SerializedName("validation")
	private String validation;

	public void setLength(int length){
		this.length = length;
	}

	public int getLength(){
		return length;
	}

	public void setValidation(String validation){
		this.validation = validation;
	}

	public String getValidation(){
		return validation;
	}

	@Override
 	public String toString(){
		return 
			"CardNumber{" + 
			"length = '" + length + '\'' + 
			",validation = '" + validation + '\'' + 
			"}";
		}
}