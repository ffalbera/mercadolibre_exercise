package com.exercisemercadolibre.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class FeeAmount {

	@SerializedName("payment_method_id")
	private String paymentMethodId;

	@SerializedName("payer_costs")
	private List<PayerCostsItem> payerCosts;

	@SerializedName("merchant_account_id")
	private Object merchantAccountId;

	@SerializedName("processing_mode")
	private String processingMode;

	@SerializedName("payment_type_id")
	private String paymentTypeId;

	@SerializedName("issuer")
	private Issuer issuer;

	public void setPaymentMethodId(String paymentMethodId){
		this.paymentMethodId = paymentMethodId;
	}

	public String getPaymentMethodId(){
		return paymentMethodId;
	}

	public void setPayerCosts(List<PayerCostsItem> payerCosts){
		this.payerCosts = payerCosts;
	}

	public List<PayerCostsItem> getPayerCosts(){
		return payerCosts;
	}

	public void setMerchantAccountId(Object merchantAccountId){
		this.merchantAccountId = merchantAccountId;
	}

	public Object getMerchantAccountId(){
		return merchantAccountId;
	}

	public void setProcessingMode(String processingMode){
		this.processingMode = processingMode;
	}

	public String getProcessingMode(){
		return processingMode;
	}

	public void setPaymentTypeId(String paymentTypeId){
		this.paymentTypeId = paymentTypeId;
	}

	public String getPaymentTypeId(){
		return paymentTypeId;
	}

	public void setIssuer(Issuer issuer){
		this.issuer = issuer;
	}

	public Issuer getIssuer(){
		return issuer;
	}

	@Override
 	public String toString(){
		return 
			"FeeAmount{" +
			"payment_method_id = '" + paymentMethodId + '\'' + 
			",payer_costs = '" + payerCosts + '\'' + 
			",merchant_account_id = '" + merchantAccountId + '\'' + 
			",processing_mode = '" + processingMode + '\'' + 
			",payment_type_id = '" + paymentTypeId + '\'' + 
			",issuer = '" + issuer + '\'' + 
			"}";
		}
}