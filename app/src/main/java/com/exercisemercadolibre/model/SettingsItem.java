package com.exercisemercadolibre.model;

import com.google.gson.annotations.SerializedName;

public class SettingsItem{

	@SerializedName("security_code")
	private SecurityCode securityCode;

	@SerializedName("card_number")
	private CardNumber cardNumber;

	@SerializedName("bin")
	private Bin bin;

	public void setSecurityCode(SecurityCode securityCode){
		this.securityCode = securityCode;
	}

	public SecurityCode getSecurityCode(){
		return securityCode;
	}

	public void setCardNumber(CardNumber cardNumber){
		this.cardNumber = cardNumber;
	}

	public CardNumber getCardNumber(){
		return cardNumber;
	}

	public void setBin(Bin bin){
		this.bin = bin;
	}

	public Bin getBin(){
		return bin;
	}

	@Override
 	public String toString(){
		return 
			"SettingsItem{" + 
			"security_code = '" + securityCode + '\'' + 
			",card_number = '" + cardNumber + '\'' + 
			",bin = '" + bin + '\'' + 
			"}";
		}
}