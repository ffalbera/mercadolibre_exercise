package com.exercisemercadolibre.model;

import com.google.gson.annotations.SerializedName;

public class Bank {

	@SerializedName("thumbnail")
	private String thumbnail;

	@SerializedName("secure_thumbnail")
	private String secureThumbnail;

	@SerializedName("name")
	private String name;

	@SerializedName("merchant_account_id")
	private Object merchantAccountId;

	@SerializedName("processing_mode")
	private String processingMode;

	@SerializedName("id")
	private String id;

	public void setThumbnail(String thumbnail){
		this.thumbnail = thumbnail;
	}

	public String getThumbnail(){
		return thumbnail;
	}

	public void setSecureThumbnail(String secureThumbnail){
		this.secureThumbnail = secureThumbnail;
	}

	public String getSecureThumbnail(){
		return secureThumbnail;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setMerchantAccountId(Object merchantAccountId){
		this.merchantAccountId = merchantAccountId;
	}

	public Object getMerchantAccountId(){
		return merchantAccountId;
	}

	public void setProcessingMode(String processingMode){
		this.processingMode = processingMode;
	}

	public String getProcessingMode(){
		return processingMode;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"Bank{" +
			"thumbnail = '" + thumbnail + '\'' + 
			",secure_thumbnail = '" + secureThumbnail + '\'' + 
			",name = '" + name + '\'' + 
			",merchant_account_id = '" + merchantAccountId + '\'' + 
			",processing_mode = '" + processingMode + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}