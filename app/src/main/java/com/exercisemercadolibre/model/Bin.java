package com.exercisemercadolibre.model;


import com.google.gson.annotations.SerializedName;

public class Bin{

	@SerializedName("pattern")
	private String pattern;

	@SerializedName("installments_pattern")
	private String installmentsPattern;

	@SerializedName("exclusion_pattern")
	private String exclusionPattern;

	public void setPattern(String pattern){
		this.pattern = pattern;
	}

	public String getPattern(){
		return pattern;
	}

	public void setInstallmentsPattern(String installmentsPattern){
		this.installmentsPattern = installmentsPattern;
	}

	public String getInstallmentsPattern(){
		return installmentsPattern;
	}

	public void setExclusionPattern(String exclusionPattern){
		this.exclusionPattern = exclusionPattern;
	}

	public String getExclusionPattern(){
		return exclusionPattern;
	}

	@Override
 	public String toString(){
		return 
			"Bin{" + 
			"pattern = '" + pattern + '\'' + 
			",installments_pattern = '" + installmentsPattern + '\'' + 
			",exclusion_pattern = '" + exclusionPattern + '\'' + 
			"}";
		}
}