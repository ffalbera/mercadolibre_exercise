package com.exercisemercadolibre.model;

import com.google.gson.annotations.SerializedName;

public class Issuer {

    @SerializedName("thumbnail")
    private String thumbnail;

    @SerializedName("secure_thumbnail")
    private String secureThumbnail;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private String id;

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setSecureThumbnail(String secureThumbnail) {
        this.secureThumbnail = secureThumbnail;
    }

    public String getSecureThumbnail() {
        return secureThumbnail;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return
                "Issuer{" +
                        "thumbnail = '" + thumbnail + '\'' +
                        ",secure_thumbnail = '" + secureThumbnail + '\'' +
                        ",name = '" + name + '\'' +
                        ",id = '" + id + '\'' +
                        "}";
    }
}