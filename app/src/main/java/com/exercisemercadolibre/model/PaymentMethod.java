package com.exercisemercadolibre.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaymentMethod {

    @SerializedName("financial_institutions")
    private List<Object> financialInstitutions;

    @SerializedName("settings")
    private List<SettingsItem> settings;

    @SerializedName("thumbnail")
    private String thumbnail;

    @SerializedName("deferred_capture")
    private String deferredCapture;

    @SerializedName("secure_thumbnail")
    private String secureThumbnail;

    @SerializedName("min_allowed_amount")
    private double minAllowedAmount;

    @SerializedName("processing_modes")
    private List<String> processingModes;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private String id;

    @SerializedName("additional_info_needed")
    private List<String> additionalInfoNeeded;

    @SerializedName("payment_type_id")
    private String paymentTypeId;

    @SerializedName("status")
    private String status;

    @SerializedName("max_allowed_amount")
    private int maxAllowedAmount;

    @SerializedName("accreditation_time")
    private int accreditationTime;

    public void setFinancialInstitutions(List<Object> financialInstitutions){
        this.financialInstitutions = financialInstitutions;
    }

    public List<Object> getFinancialInstitutions(){
        return financialInstitutions;
    }

    public void setSettings(List<SettingsItem> settings){
        this.settings = settings;
    }

    public List<SettingsItem> getSettings(){
        return settings;
    }

    public void setThumbnail(String thumbnail){
        this.thumbnail = thumbnail;
    }

    public String getThumbnail(){
        return thumbnail;
    }

    public void setDeferredCapture(String deferredCapture){
        this.deferredCapture = deferredCapture;
    }

    public String getDeferredCapture(){
        return deferredCapture;
    }

    public void setSecureThumbnail(String secureThumbnail){
        this.secureThumbnail = secureThumbnail;
    }

    public String getSecureThumbnail(){
        return secureThumbnail;
    }

    public void setMinAllowedAmount(int minAllowedAmount){
        this.minAllowedAmount = minAllowedAmount;
    }

    public double getMinAllowedAmount(){
        return minAllowedAmount;
    }

    public void setProcessingModes(List<String> processingModes){
        this.processingModes = processingModes;
    }

    public List<String> getProcessingModes(){
        return processingModes;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return id;
    }

    public void setAdditionalInfoNeeded(List<String> additionalInfoNeeded){
        this.additionalInfoNeeded = additionalInfoNeeded;
    }

    public List<String> getAdditionalInfoNeeded(){
        return additionalInfoNeeded;
    }

    public void setPaymentTypeId(String paymentTypeId){
        this.paymentTypeId = paymentTypeId;
    }

    public String getPaymentTypeId(){
        return paymentTypeId;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return status;
    }

    public void setMaxAllowedAmount(int maxAllowedAmount){
        this.maxAllowedAmount = maxAllowedAmount;
    }

    public int getMaxAllowedAmount(){
        return maxAllowedAmount;
    }

    public void setAccreditationTime(int accreditationTime){
        this.accreditationTime = accreditationTime;
    }

    public int getAccreditationTime(){
        return accreditationTime;
    }

    @Override
    public String toString(){
        return
                "Bank{" +
                        "financial_institutions = '" + financialInstitutions + '\'' +
                        ",settings = '" + settings + '\'' +
                        ",thumbnail = '" + thumbnail + '\'' +
                        ",deferred_capture = '" + deferredCapture + '\'' +
                        ",secure_thumbnail = '" + secureThumbnail + '\'' +
                        ",min_allowed_amount = '" + minAllowedAmount + '\'' +
                        ",processing_modes = '" + processingModes + '\'' +
                        ",name = '" + name + '\'' +
                        ",id = '" + id + '\'' +
                        ",additional_info_needed = '" + additionalInfoNeeded + '\'' +
                        ",payment_type_id = '" + paymentTypeId + '\'' +
                        ",status = '" + status + '\'' +
                        ",max_allowed_amount = '" + maxAllowedAmount + '\'' +
                        ",accreditation_time = '" + accreditationTime + '\'' +
                        "}";
    }
}
