package com.exercisemercadolibre.model;

import com.google.gson.annotations.SerializedName;

public class SecurityCode{

	@SerializedName("mode")
	private String mode;

	@SerializedName("card_location")
	private String cardLocation;

	@SerializedName("length")
	private int length;

	public void setMode(String mode){
		this.mode = mode;
	}

	public String getMode(){
		return mode;
	}

	public void setCardLocation(String cardLocation){
		this.cardLocation = cardLocation;
	}

	public String getCardLocation(){
		return cardLocation;
	}

	public void setLength(int length){
		this.length = length;
	}

	public int getLength(){
		return length;
	}

	@Override
 	public String toString(){
		return 
			"SecurityCode{" + 
			"mode = '" + mode + '\'' + 
			",card_location = '" + cardLocation + '\'' + 
			",length = '" + length + '\'' + 
			"}";
		}
}