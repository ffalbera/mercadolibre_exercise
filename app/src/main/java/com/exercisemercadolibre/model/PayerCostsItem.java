package com.exercisemercadolibre.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class PayerCostsItem {

    @SerializedName("installments")
    private int installments;

    @SerializedName("installment_amount")
    private double installmentAmount;

    @SerializedName("total_amount")
    private double totalAmount;

    @SerializedName("recommended_message")
    private String recommendedMessage;

    @SerializedName("installment_rate")
    private double installmentRate;

    @SerializedName("installment_rate_collector")
    private List<String> installmentRateCollector;

    @SerializedName("discount_rate")
    private int discountRate;

    @SerializedName("min_allowed_amount")
    private int minAllowedAmount;

    @SerializedName("labels")
    private List<String> labels;

    @SerializedName("max_allowed_amount")
    private int maxAllowedAmount;

    public void setInstallments(int installments) {
        this.installments = installments;
    }

    public int getInstallments() {
        return installments;
    }

    public void setInstallmentAmount(int installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public double getInstallmentAmount() {
        return installmentAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setRecommendedMessage(String recommendedMessage) {
        this.recommendedMessage = recommendedMessage;
    }

    public String getRecommendedMessage() {
        return recommendedMessage;
    }

    public void setInstallmentRate(int installmentRate) {
        this.installmentRate = installmentRate;
    }

    public double getInstallmentRate() {
        return installmentRate;
    }

    public void setInstallmentRateCollector(List<String> installmentRateCollector) {
        this.installmentRateCollector = installmentRateCollector;
    }

    public List<String> getInstallmentRateCollector() {
        return installmentRateCollector;
    }

    public void setDiscountRate(int discountRate) {
        this.discountRate = discountRate;
    }

    public int getDiscountRate() {
        return discountRate;
    }

    public void setMinAllowedAmount(int minAllowedAmount) {
        this.minAllowedAmount = minAllowedAmount;
    }

    public int getMinAllowedAmount() {
        return minAllowedAmount;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setMaxAllowedAmount(int maxAllowedAmount) {
        this.maxAllowedAmount = maxAllowedAmount;
    }

    public int getMaxAllowedAmount() {
        return maxAllowedAmount;
    }

    @Override
    public String toString() {
        return
                "PayerCostsItem{" +
                        "installments = '" + installments + '\'' +
                        ",installment_amount = '" + installmentAmount + '\'' +
                        ",total_amount = '" + totalAmount + '\'' +
                        ",recommended_message = '" + recommendedMessage + '\'' +
                        ",installment_rate = '" + installmentRate + '\'' +
                        ",installment_rate_collector = '" + installmentRateCollector + '\'' +
                        ",discount_rate = '" + discountRate + '\'' +
                        ",min_allowed_amount = '" + minAllowedAmount + '\'' +
                        ",labels = '" + labels + '\'' +
                        ",max_allowed_amount = '" + maxAllowedAmount + '\'' +
                        "}";
    }
}