package com.exercisemercadolibre.ui.fee_amount;

import com.exercisemercadolibre.model.FeeAmount;
import com.exercisemercadolibre.model.FeeAmountResponse;
import com.exercisemercadolibre.model.PayerCostsItem;
import com.exercisemercadolibre.utils.Constants;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class FeeAmountPresenterTest {

    @Test
    public void verifyThatShowProgressWhenRetrivePaymentMethodList() {
        FeeAmountContract.View mockedView = mock(FeeAmountContract.View.class);

        FeeAmountPresenter presenter = new FeeAmountPresenter(mockedView);
        presenter.retrieveFeeAmountList(100, "visa", 338);

        verify(mockedView).showProgress();
    }

    @Test
    public void noShowPaymentMethodWhenTheCallIsSucessfullButNoRetrieveData() {
        FeeAmountContract.View mockedView = mock(FeeAmountContract.View.class);

        FeeAmountPresenter presenter = new FeeAmountPresenter(mockedView);
        presenter.onSucessfull(new FeeAmountResponse());

        verify(mockedView).showNotResultFeeAmount();
    }

    @Test
    public void showBankWhenTheCallIsSucessfullAndRetriveData() {
        FeeAmountContract.View mockedView = mock(FeeAmountContract.View.class);

        FeeAmountPresenter presenter = new FeeAmountPresenter(mockedView);
        FeeAmount feeAmount = new FeeAmount();
        feeAmount.setPaymentMethodId("visa");

        PayerCostsItem payerCostsItem = new PayerCostsItem();
        payerCostsItem.setInstallments(1);
        payerCostsItem.setInstallmentRate(0);
        payerCostsItem.setRecommendedMessage(Constants.TEST_RECOMMENDED_MESSAGE);
        List<PayerCostsItem> payerCostsItems = new ArrayList<>();
        payerCostsItems.add(payerCostsItem);

        feeAmount.setPayerCosts(payerCostsItems);

        FeeAmountResponse feeAmountResponse = new FeeAmountResponse();
        feeAmountResponse.add(feeAmount);

        presenter.onSucessfull(feeAmountResponse);

        verify(mockedView).setFeeAmountList(feeAmountResponse);
    }

    @Test
    public void verifyThatHideProgressWhenRetriveBankList() {
        FeeAmountContract.View mockedView = mock(FeeAmountContract.View.class);

        FeeAmountPresenter presenter = new FeeAmountPresenter(mockedView);
        FeeAmount feeAmount = new FeeAmount();
        feeAmount.setPaymentMethodId("visa");

        PayerCostsItem payerCostsItem = new PayerCostsItem();
        payerCostsItem.setInstallments(1);
        payerCostsItem.setInstallmentRate(0);
        payerCostsItem.setRecommendedMessage(Constants.TEST_RECOMMENDED_MESSAGE);
        List<PayerCostsItem> payerCostsItems = new ArrayList<>();
        payerCostsItems.add(payerCostsItem);

        feeAmount.setPayerCosts(payerCostsItems);

        FeeAmountResponse feeAmountResponse = new FeeAmountResponse();
        feeAmountResponse.add(feeAmount);

        presenter.onSucessfull(feeAmountResponse);

        verify(mockedView).hideProgress();
    }

    @Test
    public void verifyThatHideProgressWhenRetriveIsFailure() {
        FeeAmountContract.View mockedView = mock(FeeAmountContract.View.class);

        FeeAmountPresenter presenter = new FeeAmountPresenter(mockedView);
        presenter.onFailure(new Throwable());

        verify(mockedView).hideProgress();
    }

    @Test
    public void verifyThatCallFailureWhenRetrieveIsFailure() {
        FeeAmountContract.View mockedView = mock(FeeAmountContract.View.class);

        FeeAmountPresenter presenter = new FeeAmountPresenter(mockedView);
        Throwable throwable = new Throwable();
        presenter.onFailure(throwable);

        verify(mockedView).onResponseFailure(throwable);
    }
}
