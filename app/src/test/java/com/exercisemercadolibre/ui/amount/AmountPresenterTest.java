package com.exercisemercadolibre.ui.amount;

import com.exercisemercadolibre.utils.Constants;

import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class AmountPresenterTest {

    @Test
    public void verifyThatEnabledButtonNextWhenEnterAmount() {
        AmountContract.View mockedView =  mock(AmountContract.View.class);

        AmountPresenter presenter = new AmountPresenter(mockedView);
        presenter.verifyAmount(Constants.TEST_AMOUNT);

        verify(mockedView).enableButtonNext();
    }

    @Test
    public void verifyThatDisabledButtonNextWhenNotEnterAmount() {
        AmountContract.View mockedView =  mock(AmountContract.View.class);

        AmountPresenter presenter = new AmountPresenter(mockedView);
        presenter.verifyAmount(Constants.EMPTY_STRING);

        verify(mockedView).disableButtonNext();
    }

}
