package com.exercisemercadolibre.ui.payment_method;

import com.exercisemercadolibre.model.PaymentMethod;
import com.exercisemercadolibre.model.PaymentMethodResponse;

import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class PaymentMethodPresenterTest {

    @Test
    public void verifyThatShowProgressWhenRetrivePaymentMethodList() {
        PaymentMethodContract.View mockedView =  mock(PaymentMethodContract.View.class);

        PaymentMethodPresenter presenter = new PaymentMethodPresenter(mockedView);
        presenter.retrievePaymentMethodList();

        verify(mockedView).showProgress();
    }

    @Test
    public void noShowPaymentMethodWhenTheCallIsSucessfullButNoRetrieveData() {
        PaymentMethodContract.View mockedView = mock(PaymentMethodContract.View.class);

        PaymentMethodPresenter presenter = new PaymentMethodPresenter(mockedView);
        presenter.onSucessfull(new PaymentMethodResponse());

        verify(mockedView).showNotResultPaymentMethod();
    }

    @Test
    public void showBankWhenTheCallIsSucessfullAndRetriveData() {
        PaymentMethodContract.View mockedView = mock(PaymentMethodContract.View.class);

        PaymentMethodPresenter presenter = new PaymentMethodPresenter(mockedView);
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setId("visa");
        paymentMethod.setName("Visa");
        paymentMethod.setPaymentTypeId("credit_card");
        paymentMethod.setStatus("active");

        PaymentMethodResponse paymentMethodResponse = new PaymentMethodResponse();
        paymentMethodResponse.add(paymentMethod);

        presenter.onSucessfull(paymentMethodResponse);

        verify(mockedView).setPaymentMethodList(paymentMethodResponse);
    }

    @Test
    public void verifyThatHideProgressWhenRetriveBankList() {
        PaymentMethodContract.View mockedView = mock(PaymentMethodContract.View.class);

        PaymentMethodPresenter presenter = new PaymentMethodPresenter(mockedView);
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setId("visa");
        paymentMethod.setName("Visa");
        paymentMethod.setPaymentTypeId("credit_card");
        paymentMethod.setStatus("active");

        PaymentMethodResponse paymentMethodResponse = new PaymentMethodResponse();
        paymentMethodResponse.add(paymentMethod);

        presenter.onSucessfull(paymentMethodResponse);

        verify(mockedView).hideProgress();
    }

    @Test
    public void verifyThatHideProgressWhenRetriveIsFailure() {
        PaymentMethodContract.View mockedView = mock(PaymentMethodContract.View.class);

        PaymentMethodPresenter presenter = new PaymentMethodPresenter(mockedView);

        presenter.onFailure(new Throwable());

        verify(mockedView).hideProgress();
    }

    @Test
    public void verifyThatCallFailureWhenRetrieveIsFailure() {
        PaymentMethodContract.View mockedView = mock(PaymentMethodContract.View.class);

        PaymentMethodPresenter presenter = new PaymentMethodPresenter(mockedView);

        Throwable throwable = new Throwable();
        presenter.onFailure(throwable);

        verify(mockedView).onResponseFailure(throwable);
    }

}
