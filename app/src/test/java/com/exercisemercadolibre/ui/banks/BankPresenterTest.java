package com.exercisemercadolibre.ui.banks;

import com.exercisemercadolibre.model.Bank;
import com.exercisemercadolibre.model.BankResponse;
import com.exercisemercadolibre.utils.Constants;

import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class BankPresenterTest {

    @Test
    public void verifyThatShowProgressWhenRetriveBankList() {
        BankContract.View mockedView = mock(BankContract.View.class);

        BankPresenter presenter = new BankPresenter(mockedView);
        presenter.retrieveBankList(Constants.TEST_PAYMENT_METHOD_ID);

        verify(mockedView).showProgress();
    }

    @Test
    public void noShowBankWhenTheCallIsSucessfullButNoRetrieveData() {
        BankContract.View mockedView = mock(BankContract.View.class);

        BankPresenter presenter = new BankPresenter(mockedView);
        presenter.onSucessfull(new BankResponse());

        verify(mockedView).showNotResultBank();
    }

    @Test
    public void showBankWhenTheCallIsSucessfullAndRetriveData() {
        BankContract.View mockedView = mock(BankContract.View.class);

        BankPresenter presenter = new BankPresenter(mockedView);
        Bank bank = new Bank();
        bank.setId("288");
        bank.setName("Tarjeta Shopping");
        bank.setSecureThumbnail("https://www.mercadopago.com/org-img/MP3/API/logos/288.gif");
        bank.setThumbnail("http://img.mlstatic.com/org-img/MP3/API/logos/288.gif");
        bank.setProcessingMode("aggregator");
        BankResponse bankResponse = new BankResponse();
        bankResponse.add(bank);

        presenter.onSucessfull(bankResponse);

        verify(mockedView).setBankList(bankResponse);
    }

    @Test
    public void verifyThatHideProgressWhenRetriveBankList() {
        BankContract.View mockedView = mock(BankContract.View.class);

        BankPresenter presenter = new BankPresenter(mockedView);
        Bank bank = new Bank();
        bank.setId("288");
        bank.setName("Tarjeta Shopping");
        bank.setSecureThumbnail("https://www.mercadopago.com/org-img/MP3/API/logos/288.gif");
        bank.setThumbnail("http://img.mlstatic.com/org-img/MP3/API/logos/288.gif");
        bank.setProcessingMode("aggregator");
        BankResponse bankResponse = new BankResponse();
        bankResponse.add(bank);

        presenter.onSucessfull(bankResponse);

        verify(mockedView).hideProgress();
    }

    @Test
    public void verifyThatHideProgressWhenRetriveIsFailure() {
        BankContract.View mockedView = mock(BankContract.View.class);

        BankPresenter presenter = new BankPresenter(mockedView);

        presenter.onFailure(new Throwable());

        verify(mockedView).hideProgress();
    }

    @Test
    public void verifyThatCallFailureWhenRetrieveIsFailure() {
        BankContract.View mockedView = mock(BankContract.View.class);

        BankPresenter presenter = new BankPresenter(mockedView);

        Throwable throwable = new Throwable();
        presenter.onFailure(throwable);

        verify(mockedView).onResponseFailure(throwable);
    }
}
